/*
 * pthread_util.h
 *
 *  Created on: Apr 15, 2014
 *      Author: galor
 */

#ifndef PTHREAD_UTIL_H_
#define PTHREAD_UTIL_H_
#include <sched.h>
#define THR_PRI_MAX                 sched_get_priority_max(SCHED_FIFO)
#define THR_PRI_MIN                 sched_get_priority_min(SCHED_FIFO)

#define THR_PRI_DEFAULT             (THR_PRI_MIN + (THR_PRI_MAX-THR_PRI_MIN) / 2)

#define THR_STACK_SIZE_DEFAULT      0
#define SYS_ERROR "system error\n"
#define DEVICE_ERROR "Output device library error\n"
typedef void * (*ThrEntryFunc)(void *);

/*
 * makes the program block during of msecs
 */
void  thread_sleep(unsigned msecs);
/**
 * create a new thread
 */
void * create_thread(ThrEntryFunc entryFunc, int thr_priority, void *thr_prm);
/**
 * delete a thread
 */
int delete_thread(void * p_hndl);
/*
 * create mutex
 */
void * create_mutex();
/*
 * delete mutex
 */
void delete_mutext(void * h_mutex);
/*
 * lock the mutex
 */
void lock_mutex(void * h_mutex);
/*
 * unlock the mutex
 */
void unlock_mutex(void * h_mutex);

/*
 * this struct defines a queue, that use in multi_thread programming
 */
typedef struct {
  unsigned put_idx;
  unsigned get_idx;
  unsigned len;
  unsigned count;
  int *queue;
  void * h_lock;
} Queue;
/**
 * create new queue, the queue len(size) is maxLen,
 *  return a pointer to the queue on success, else return NULL
 */

Queue * create_queue(unsigned maxLen);
/*
 * delete the queue
 */
int delete_queue(Queue *hq);
/**
 * insert new value to the end of the queue, during the insertion mutex in lock
 * return 0 on success, -1 on fail
 */
int insert_queue(Queue *hq, int value);
/**
 * remove the first value of the queue, during the removal, mutex is lock
 */
int remove_queue(Queue *hq );
/**
 * return 1 if queue is empty, 0 otherwise
 */
int queue_is_empty(Queue *hq);


#endif /* PTHREAD_UTIL_H_ */
