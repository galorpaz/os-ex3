/*
 * outputdevice.cpp
 *
 *  Created on: Apr 13, 2014
 *      Author: galor
 */
#include "outputdevice.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "pthread_util.h"
#include <limits.h>


#ifndef INT_MAX
#define INT_MAX   (10)
#endif

#ifndef INT_MIN
#define INT_MIN (-1)
#endif
#define TASKS_SIZE 100000
#define SYS_ERROR "system error\n"
#define DEVICE_ERROR "Output device library error\n"

#define _DBG
/**
 * this enum defines the system status
 */
typedef enum {
	Sys_NotInitialized,
	Sys_Initialized,
	Sys_ShuttingDown,
	Sys_Closed
} SystemStatusType;

/**
 *this enum defines Yes and No values that are used in wasItWritten function
 */
typedef enum {
	YES,
	NO
} YesNoType;

/**
 * this enum defines a task status
 */
typedef enum {
	TS_free,
	TS_occupied,
	TS_ready,
	TS_written
} TaskStatus;

/**
 * this enum defines the daemon status
 */
typedef enum {
	DMN_NUll,
	DMN_Running,
	DMN_RequestForTermination
} daemonStatesType;

/**
 * this structure defines a task
 */
typedef struct {
	volatile TaskStatus status;
	char 	*p_buffer;
	int		buf_length;
} task_type;
/*
 * this variable save the saves the system status
 */
static SystemStatusType sys_status = Sys_NotInitialized;
/**
 * this variable represent the queue of the tasks
 */
static task_type tasks[TASKS_SIZE];
/**
 * this queue holds the task id of tasks that needs to be written
 */
static Queue *h_write_que = NULL;
/*
 * this variable is initialize to 0 and increment each time a task writs to the file
 */
static int num_of_writes = 0;
/*
 *this variable is used in order to check if initDevice was called
 *it get the fid once the out put file is open
 */
static FILE *ddfile_handle = NULL;
/*
 * this variable is used in order to control mutex
 */
static void* h_mutex = NULL;
/*
 * this variable is used in order to control the daemon thread
 */
static void* h_daemon = NULL;
/*
 * this variable is used in order to know the daemon status
 */
static volatile daemonStatesType daemon_state = DMN_NUll;

/**
 * this function initialize the task array
 * put "TS_free" task status for each task_id, indicates that this task is available for use
 */
static void init_tasks() {
	int i;
	for(i=0; i<TASKS_SIZE; ++i) {
		tasks[i].status = TS_free;
		tasks[i].p_buffer = NULL;
		tasks[i].buf_length = 0;
	}

	num_of_writes = 0;
}
/**
 * this function fund the minimum available task_id
 */
static int get_min_free_task_id() {
	int ret_val = -1;
	int new_idx;
	lock_mutex(h_mutex);
	for (new_idx = 0; new_idx <= TASKS_SIZE; ++new_idx) {
		if (tasks[new_idx].status == TS_free || tasks[new_idx].status == TS_written) {
			tasks[new_idx].status = TS_occupied;
			ret_val = new_idx;
			break;
		}
	}
	unlock_mutex(h_mutex);
	return ret_val;
}


/**
 * this function allocates memory for the buffer in the task and update the status if the
 * task is ready
 * @param: 	buffer - the data to write
 * 			length - the length of the buffer
 * 			task_id - the index of the task id
 * @return -1 if the memory allocation failed
 * 			task_id if the memory allocation succeed
 */
static int add_buffer_to_task(char * buffer, int length, int task_id) {
	tasks[task_id].p_buffer = (char *)malloc(length);
	if (tasks[task_id].p_buffer == NULL) {
		//perror("memmory allocation");
		perror(SYS_ERROR);
		return -1;
	}
	//if allocation sucessded, we need to update the task parameters
	memcpy(tasks[task_id].p_buffer, buffer, length);
	tasks[task_id].buf_length = length;
	tasks[task_id].status = TS_ready;
	return task_id;
}
/**
 * this function write to the file
 * @param - the task id that need to be write
 * @return 0 if write success. >0 if write was failed
 */
static int write_task(int task_idx){
	int ret_val = 0;
	if (tasks[task_idx].status != TS_ready || tasks[task_idx].p_buffer == NULL || tasks[task_idx].buf_length == 0) {
		// if one of the above is exist, means there is an error
		//		perror("logic error");
		perror(DEVICE_ERROR);
		exit (-1);
	}
	ret_val = fwrite (tasks[task_idx].p_buffer, sizeof(char), tasks[task_idx].buf_length, ddfile_handle);
	fflush(ddfile_handle);
	if (ret_val < tasks[task_idx].buf_length){
		//check that write was success
		ret_val = -1; // to indicate error
		perror(SYS_ERROR);
	}
	//free the buffer after it was written
	lock_mutex(h_mutex); {
		free (tasks[task_idx].p_buffer);
		//update the task parameters
		tasks[task_idx].p_buffer = NULL;
		tasks[task_idx].buf_length = 0;
		tasks[task_idx].status = TS_written;
	} unlock_mutex(h_mutex);
	return ret_val;
}

/**
 * this function manage the daemon operation
 *  @param - p_prm, the parameter returns when creating the thread
 *  @return -NULL, indicate the daemon finish its work
 */
void * daemon_thread(void * p_prm) {
	// we need volatile in order to suppress optimization on this variable, because it can be
	//change out from this scope
	volatile daemonStatesType *p_state = (daemonStatesType *) p_prm;
	int ret_val = 0;
	int queue_empty=0;
	int exit_flag = 0;
	int write_idx;
	*p_state = DMN_Running;
	do  {
		while (queue_is_empty(h_write_que)) {
			//the queue is empty
			if (*p_state == DMN_RequestForTermination ) //there is nothing to write and request for closing accrue.
			{
				exit_flag = 1;
				break;
			}
			thread_sleep(5);
		}
		queue_empty = queue_is_empty(h_write_que);

		if (exit_flag && queue_empty)
			break;

		write_idx = remove_queue(h_write_que);
		if (write_idx < 0) {
//			perror("remove_failed\n");
			perror(DEVICE_ERROR);
			break;
		}
		if (tasks[write_idx].status != TS_ready) { // when task is not empty it means
			// that task status should be ready
//			perror("logic error\n");
			perror(DEVICE_ERROR);
			break;
		}

		// there is something to write
		ret_val = write_task(write_idx);//writing to the file

		//check if write was success
		if (ret_val < 0) {
			//			perror ("write failed");
			perror(SYS_ERROR);
			break;
		}
		++num_of_writes;

	} while (exit_flag == 0) ;

	//initialize the daemon thread
	*p_state = DMN_NUll;
	return NULL;
}

/*
 * DESCRIPTION: The function creates the file filename if it does not exist and open the file for writing.
 *              This function should be called prior to any other functions as a necessary precondition for their success.
 *
 * RETURN VALUE: On success 0, -2 for a filesystem error (inability to create the file, etc.), otherwise -1.
 */
int initdevice(char *filename) {

	int ret_val = 0;
	if (ddfile_handle == NULL) {
		//file was not initialized
		ddfile_handle = fopen(filename , "a+");
		if (ddfile_handle == NULL) {
			//open command failed
			//			perror("unable to open outputfile");
			perror (SYS_ERROR);
			ret_val = -2;
			goto exit_on_error;
		}
		//Initialize the task array
		init_tasks();
		//Initialize the mutex
		h_mutex = create_mutex();
		if (h_mutex == NULL) {
			//mutex creation failed
			//			perror("failed to create h_mutex");
			perror(SYS_ERROR);
			ret_val = -1;
			goto exit_on_error;
		}
		//initialize the queue
		h_write_que = create_queue(TASKS_SIZE+1);
		if (h_write_que == NULL) {
			// initialize failed
			perror(SYS_ERROR);
			ret_val =-1;
			goto exit_on_error;
		}
		/*
		 * from this point the function daemon_thread start running
		 */
		h_daemon = create_thread(daemon_thread, THR_PRI_DEFAULT, (void*) &daemon_state);
		if (h_daemon != NULL) {
			//wait until the daemon is created, which mean that it did not changed is status to DMN_Running
			while (daemon_state != DMN_Running)
				thread_sleep(10);
			sys_status = Sys_Initialized;
		}
		else {
			//			perror("Failed to create daemon\n");
			perror(SYS_ERROR);
			ret_val = -1;
			goto exit_on_error;
		}

	}
	else {
		//file was already initialized
		//		perror("init device as already been called");
		perror(DEVICE_ERROR);
		ret_val = -1;
	}
	return ret_val;
	//in case that error appear, all open instance, should be closed before exiting the program
	exit_on_error:
	if (h_mutex) {
		delete_mutext(h_mutex);
		h_mutex = NULL;
	}
	ret_val = fclose(ddfile_handle);
	if (ret_val != 0) {
		//close failed
		perror(SYS_ERROR);
	}
	ddfile_handle = NULL;

	return ret_val;
}

/*
 * DESCRIPTION: The function writes the input buffer to the file. The buffer may be freed once this call returns.
 *              You should deal with any memory management issues.
 * 		Note this is non-blocking package you are required to implement you should return ASAP,
 * 		even if the buffer has not yet been written to the disk.
 *
 * RETURN VALUE: On success, the function returns a task_id (>= 0), which identifies this write operation.
 * 		 Note, you should reuse task_ids when they become available.  On failure, -1 will be returned.
 */
int write2device(char *buffer, int length) {
	int task_id = -1;
	int ret_val = -1;
	if (sys_status == Sys_Initialized) {
		task_id = get_min_free_task_id();
		if (task_id >= 0) {
			task_id = add_buffer_to_task(buffer,length, task_id );
			if (task_id >=0 ){
				ret_val = insert_queue(h_write_que,task_id);
			}
			if (ret_val < 0){
				perror(DEVICE_ERROR);
			}

		}
	}
	else
		//		perror("initdevice should be called first");
		perror(DEVICE_ERROR);
	return task_id;
}

/*
 * DESCRIPTION: Block until the specified task_id has been written to the file.
 * 		The task_id is a value that was previously returned by write2device function.
 * 		In case of task_id doesn't exist, should return -2; In case of other errors, return -1.
 *
 */
int flush2device(int task_id) {
	int ret_val = task_id;
	if (sys_status == Sys_Initialized)
	{
		if (task_id < 0 || task_id > TASKS_SIZE || tasks[task_id].status == TS_free || tasks[task_id].status == TS_occupied) {
			//task_id doesn't exist or not ready
			perror(DEVICE_ERROR);
			ret_val = -2;
			goto exit_on_error;
		}
		if (tasks[task_id].status == TS_written) goto exit_on_error;

		while (tasks[task_id].status == TS_ready ) {
			//wait until tasks[task_id].status will be change
			thread_sleep (10);
			continue;
		}

	}
	else {
		//		perror("initdevice should be called first");
		perror(DEVICE_ERROR);
		ret_val = -1;
	}

	exit_on_error:
	return ret_val;
}


/*
 * DESCRIPTION: return (without blocking) whether the specified task_id has been written to the file
 *      (0 if yes, 1 if not).
 * 		The task_id is a value that was previously returned by write2device function.
 * 		In case of task_id doesn't exist, should return -2; In case of other errors, return -1.
 *
 */
int wasItWritten(int task_id){

	int ret_val = 0;
	if (sys_status != Sys_Initialized ) {
		//		perror("initdevice was not called");
		perror (DEVICE_ERROR);
		ret_val = -1;
		goto exit_on_error;

	}
	if (task_id >= TASKS_SIZE || tasks[task_id].status == TS_free) {
		//		perror("task dose not exist");
		perror(DEVICE_ERROR);
		ret_val = -2;
		goto exit_on_error;
	}
	if (tasks[task_id].status == TS_written) {
		ret_val = YES;
	}
	else {
		ret_val = NO;
	}

	exit_on_error:
	return ret_val;
}


/*
 * DESCRIPTION: return (without blocking) how many tasks have been written to file since last initdevice.
 *      If number exceeds MAX_INT, return MIN_INT.
 * 		In case of error, return -1.
 *
 */
int howManyWritten(){
	int ret_val = 0;
	if (sys_status == Sys_Initialized){
		ret_val = num_of_writes;
		if (ret_val > INT_MAX) 	ret_val = INT_MIN;

	}
	else {
		//		perror("initDevice was not called");
		perror(DEVICE_ERROR);
		ret_val = -1;
	}

	return ret_val;
}



/*
 * DESCRIPTION: close the output file and reset the system so that it is possible to call initdevice again.
 *              All pending task_ids should be written to output disk file.
 *              Any attempt to write new buffers (or initialize) while the system is shutting down should
 *              cause an error.
 *              In case of error, the function should cause the process to exit.
 *
 */
void closedevice(){
	if (sys_status == Sys_Initialized) {
		sys_status = Sys_ShuttingDown;
		// request daemon for termination
		if (h_daemon != NULL)
		{
			daemon_state = DMN_RequestForTermination;
			//wait until the daemon will terminate
			while( daemon_state != DMN_NUll )
				thread_sleep(10);
		}

		if (! queue_is_empty(h_write_que) ) {
			//system cannot be close if there are still tasks to write
			//			perror("Logic error daemon should be null");
			perror(DEVICE_ERROR);
		}
		//initialize the system parameter, so they will be ready for a second use
		fflush(ddfile_handle);
		fclose (ddfile_handle);
		ddfile_handle = NULL;

		// delete all instances
		if (h_daemon != NULL)
			delete_thread(h_daemon);
		h_daemon = NULL;
		delete_mutext(h_mutex);
		h_mutex = NULL;
		delete_queue(h_write_que);
		h_write_que = NULL;
		sys_status = Sys_Closed;

	}
	else {
		//		perror ("output device was not initialized");
		perror (DEVICE_ERROR);
		//	exit(0);
	}
}

/*
 * DESCRIPTION: Wait for closedevice to finish.
 *              If closing was successful, it returns 1.
 *              If closedevice was not called it should return -2.
 *              In case of other error, it should return -1.
 *
 */
int wait4close() {
	int ret_val = 1;
	if (sys_status == Sys_NotInitialized) {
		//		perror("output device was not initialized");
		perror(DEVICE_ERROR);
		ret_val = -1;
		goto exit_on_error;
	}

	if (sys_status == Sys_Initialized) {
		//		perror("closedevice has not been called");
		perror(DEVICE_ERROR);
		ret_val = -2;
		goto exit_on_error;
	}

	while(sys_status == Sys_ShuttingDown) {
		thread_sleep(10);
	}
	exit_on_error:
	return ret_val;
}
