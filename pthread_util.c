/*
 * pthread_util.c
 *
 *  Created on: Apr 14, 2014
 *      Author: galor
 */
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <time.h>
#include <sys/time.h>



#include "pthread_util.h"
/**
 *
 */
void  thread_sleep(unsigned msecs)
{
	struct timespec delayTime, elaspedTime;

	delayTime.tv_sec  = (__time_t) (msecs/1000U);
	delayTime.tv_nsec = (msecs%1000U)*1000000L;

	nanosleep(&delayTime, &elaspedTime);
}
/**
 *
 */
void * create_thread(ThrEntryFunc entryFunc, int thr_priority, void *thr_prm)
{
	//	printf("create_thread");
	int status = 0;
	pthread_t *p_hndl = NULL; //the return value of the function, this is the identifier of the thread
	pthread_attr_t thread_attr; //we need this in order to creat the thread

	// initialize thread attributes structure
	status = pthread_attr_init(&thread_attr);

	if(status != 0) { // error with pthread_attr_init
		//		perror("create_thread() - Could not initialize thread attributes\n");
		perror(SYS_ERROR);
		return NULL;
	}

	status = pthread_attr_setinheritsched(&thread_attr, PTHREAD_INHERIT_SCHED);

	// check that the input value of priority is in the legal range
	if(thr_priority > THR_PRI_MAX)
		thr_priority = THR_PRI_MAX;
	else {
		if(thr_priority < THR_PRI_MIN)
			thr_priority = THR_PRI_MIN;
	}
	//set the parameter used in pthread_attr_setschedparam

	if (status != 0) { // error accrued in one of the above functionד
		//		perror("Could not initialize thread attributes");
		perror(SYS_ERROR);
		goto error_exit;
	}

	//Initialize the thread handle
	p_hndl = (pthread_t *) malloc(sizeof(pthread_t));
	if (p_hndl == NULL) // error in malloc
		goto error_exit;

	// crating the thread
	status = pthread_create(p_hndl, NULL, entryFunc, thr_prm);

	if(status != 0) { //error in pthread_create
		//		perror("create_thread() - Could not create thread\n");
		perror(SYS_ERROR);
		free(p_hndl);
		p_hndl = NULL;
	}

	error_exit:
	status = pthread_attr_destroy(&thread_attr);

	return (void *)p_hndl;
}

/**
 *
 */
int delete_thread(void * p_hndl)
{
	void *returnVal=0;
	int status;
	status = pthread_cancel((pthread_t)p_hndl);
	if (status == 0)
		status = pthread_join((pthread_t)p_hndl, &returnVal);
	free(p_hndl);
	return status;
}

/**
 *
 */
void * create_mutex()
{
	pthread_mutex_t* h_mutex = NULL;
	int status = 0;

	h_mutex = (pthread_mutex_t *)malloc (sizeof(pthread_mutex_t));
	if (h_mutex == NULL) {
		//		perror ("error with malloc h_mutex");
		perror(SYS_ERROR);

	}
	status |= pthread_mutex_init(h_mutex, NULL);

	if(status!=0){
		//		perror("pthread_mutex_init failed");
		perror (SYS_ERROR);
		free (h_mutex);
		h_mutex = NULL;
	}

	return (void *)h_mutex;
}
/**
 *
 */
void delete_mutext(void * h_mutex)
{
	pthread_mutex_destroy((pthread_mutex_t *) h_mutex);
	free (h_mutex);
	h_mutex = NULL;
}
/**
 *
 */
void lock_mutex(void * h_mutex)
{
	pthread_mutex_lock((pthread_mutex_t *) h_mutex);
}
/**
 *
 */
void unlock_mutex(void * h_mutex)
{
	pthread_mutex_unlock((pthread_mutex_t *) h_mutex);
}

/**
 * create new queue, the queue len(size) is maxLen,
 *  return a pointer to the queue on success, else return NULL
 */
Queue * create_queue(unsigned maxLen)
{
	Queue *hq = (Queue *)malloc(sizeof(Queue));
	if (hq == NULL) {
//		perror("Failed to create Queue\n");
		perror(SYS_ERROR);
		return NULL;
	}

	hq->put_idx = hq->get_idx = 0;
	hq->count = 0;
	hq->len   = maxLen;
	hq->queue = (int *)malloc(sizeof(int)*hq->len);

	if(hq->queue==NULL) {
//		perror("Failed to alloc queue data\n");
		perror(SYS_ERROR);
		return NULL;
	}
	hq->h_lock = create_mutex();

	return hq;
}
/**
 * delete the queue
 */
int delete_queue(Queue *hq)
{
	if(hq->queue!=NULL)
		free(hq->queue);
	if (hq->h_lock != NULL)
		delete_mutext(hq->h_lock);
	free(hq);
	return 0;
}
/**
 * insert new value to the end of the queue, during the insertion mutex in lock
 * return 0 on success, -1 on fail
 */
int insert_queue(Queue *hq, int value)
{
	int status = -1;

	lock_mutex(hq->h_lock);

	if( hq->count < hq->len ) {
		hq->queue[hq->put_idx] = value;
		hq->put_idx = (hq->put_idx+1)%hq->len;
		hq->count++;
		status = 0;
	}

	unlock_mutex(hq->h_lock);

	return status;
}
/**
 * remove the first value of the queue, during the removal, mutex is lock
 */
int remove_queue(Queue *hq)
{
	int ret_val = -1;

	lock_mutex(hq->h_lock);

	if(hq->count > 0 ) {
		ret_val = hq->queue[hq->get_idx];
		hq->get_idx = (hq->get_idx+1)%hq->len;
		hq->count--;
	}

	unlock_mutex(hq->h_lock);

	return ret_val;
}
/**
 * return 1 if queue is empty, 0 otherwise
 */
int queue_is_empty(Queue *hq)
{
	int is_empt;
	lock_mutex(hq->h_lock);
	is_empt = hq->count == 0;
	unlock_mutex(hq->h_lock);
	return is_empt;
}
