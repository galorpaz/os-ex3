VERBOSE=1
CC=g++
FLAGS=-Wall -lpthread

liboutputdevice.a: outputdevice.h outputdevice.c pthread_util.h pthread_util.c
	$(CC) $(FLAGS) -c pthread_util.c outputdevice.c  
	ar rvs liboutputdevice.a pthread_util.o outputdevice.o

all: liboutputdevice.a

tar:
	tar -cvf ex3.tar README Makefile outputdevice.c outputdevice.h pthread_util.c pthread_util.h

clean:
	rm -f *.o *.a *~ ex3.tar

.PHONY: all tar clean

